import sys
import cv2
import numpy as np
import os
import re
from PIL import Image
from PIL.ImageQt import ImageQt
from PyQt5 import QtWidgets
from PyQt5.QtGui import QPixmap
import SurdoTranslator as ST
import keras
from keras import backend as K
from keras.preprocessing.image import ImageDataGenerator, load_img, img_to_array
from keras.models import Sequential, load_model, model_from_json
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D

(major_ver, minor_ver, subminor_ver) = (cv2.__version__).split('.')
MODEL_FILE = os.path.join('C:\/Users\Minatych\source\/repos\surdo\scripts\model', 'mega_model.hdf5')
hand_model = load_model(MODEL_FILE, compile=False)
classes = {
    0: 'А',
    1: 'Б',
    2: 'В',
    3: 'Е',
    4: 'Э',
    5: 'Ф',
    6: 'Г',
    7: 'Х',
    8: 'И',
    9: 'Я',
    10: 'Л',
    11: 'М',
    12: 'Н',
    13: 'О',
    14: 'П',
    15: 'Р',
    16: 'С',
    17: 'Ш',
    18: 'Т',
    19: 'У',
    20: 'В',
    21: 'Х',
    22: 'Ю'
}

class SurdoTranslator(QtWidgets.QMainWindow, ST.Ui_MainWindow):
	def __init__(self):
		super().__init__()
		self.setupUi(self)
		self.isStart = False
		self.video = cv2.VideoCapture(0)
		self.labelInfo.hide()
		self.startVideo.clicked.connect(self.startTranslator)
		
	def setup_tracker(self,ttype):
		tracker_types = ['BOOSTING', 'MIL', 'KCF', 'TLD', 'MEDIANFLOW', 'GOTURN']
		tracker_type = tracker_types[ttype]
		if int(minor_ver) < 3:
			tracker = cv2.Tracker_create(tracker_type)
		else:
			if tracker_type == 'BOOSTING':
				tracker = cv2.TrackerBoosting_create()
			if tracker_type == 'MIL':
				tracker = cv2.TrackerMIL_create()
			if tracker_type == 'KCF':
				tracker = cv2.TrackerKCF_create()
			if tracker_type == 'TLD':
				tracker = cv2.TrackerTLD_create()
			if tracker_type == 'MEDIANFLOW':
				tracker = cv2.TrackerMedianFlow_create()
			if tracker_type == 'GOTURN':
				tracker = cv2.TrackerGOTURN_create()
		return tracker

	def mask_array(self, array, imask):
		if array.shape[:2] != imask.shape:
			raise Exception("Shapes of input and imask are incompatible")
		output = np.zeros_like(array, dtype=np.uint8)
		for i, row in enumerate(imask):
			output[i, row] = array[i, row]
		return output

	def stopTranslator(self):
		if self.isStart == True:
			self.labelInfo.hide()
			self.isStart = False
			cv2.destroyAllWindows()

	def startTranslator(self):
		if self.isStart == False:
			self.labelInfo.show()
			self.isStart = True
			ok, frame = self.video.read()
			bg = frame.copy()
			kernel = np.ones((3, 3), np.uint8)
			positions = {
				'hand_pose': (15, 40),
				'fps': (15, 20),
				'null_pos': (200, 200)
			}
			bbox_initial = (116, 116, 170, 170)
			bbox = bbox_initial
			tracking = -1
			while True:
				ok, frame = self.video.read()
				display = frame.copy()
				diff = cv2.absdiff(bg, frame)
				mask = cv2.cvtColor(diff, cv2.COLOR_BGR2GRAY)
				th, thresh = cv2.threshold(mask, 10, 255, cv2.THRESH_BINARY)
				opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel)
				closing = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel)
				img_dilation = cv2.dilate(closing, kernel, iterations=2)
				imask = img_dilation > 0
				foreground = self.mask_array(frame, imask)
				if tracking != -1:
					tracking, bbox = tracker.update(foreground)
					tracking = int(tracking)
				hand_crop = frame[int(bbox[1]):int(bbox[1] + bbox[3]), int(bbox[0]):int(bbox[0] + bbox[2])]
				hand_crop = cv2.cvtColor(hand_crop, cv2.COLOR_BGR2GRAY)
				try:
					hand_crop_resized = np.expand_dims(cv2.resize(hand_crop, (54, 54)), axis=0).reshape((1, 54, 54, 1))
					prediction = hand_model.predict(hand_crop_resized)
					predi = prediction[0].argmax()
					gesture = classes[predi]
					for i, pred in enumerate(prediction[0]):
						barx = positions['hand_pose'][0]
						bary = 60 + i * 60
						bar_height = 20
						bar_length = int(400 * pred) + barx
						if i == predi:
							colour = (0, 255, 0)
						else:
							colour = (0, 0, 255)					
					self.labelResult.setText('text: ' + gesture)
					#cv2.putText(display, "text: {}".format(gesture), positions['hand_pose'],cv2.FONT_HERSHEY_SIMPLEX, 0.75,(0, 0, 255), 2)
				except Exception as ex:
					print('Error');
					#cv2.putText(display, "text: error", positions['hand_pose'], cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0, 0, 255), 2)
				p1 = (int(bbox[0]), int(bbox[1]))
				p2 = (int(bbox[0] + bbox[2]), int(bbox[1] + bbox[3]))
				cv2.rectangle(display, p1, p2, (255, 0, 0), 2, 1)
				hand_pos = ((p1[0] + p2[0]) // 2, (p1[1] + p2[1]) // 2)
				mouse_change = ((p1[0] + p2[0]) // 2 - positions['null_pos'][0], positions['null_pos'][0] - (p1[1] + p2[1]) // 2)
				cv2.circle(display, positions['null_pos'], 5, (0, 0, 255), -1)
				cv2.circle(display, hand_pos, 5, (0, 255, 0), -1)
				cv2.line(display, positions['null_pos'], hand_pos, (255, 0, 0), 5)
				cv2.imshow("display", display)
				k = cv2.waitKey(1) & 0xff
				if k == ord('q'):
					self.stopTranslator()
					break
				elif k == ord('t'):
        				tracker = self.setup_tracker(2)
        				tracking = tracker.init(frame, bbox)
					
def main():
	app = QtWidgets.QApplication(sys.argv)
	window = SurdoTranslator()
	window.show()
	app.exec_()

if __name__ == '__main__':
	main()
