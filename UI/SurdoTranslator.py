# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'SurdoTranslator.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(423, 127)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.startVideo = QtWidgets.QPushButton(self.centralwidget)
        self.startVideo.setGeometry(QtCore.QRect(10, 10, 281, 41))
        self.startVideo.setObjectName("startVideo")
        self.labelInfo = QtWidgets.QLabel(self.centralwidget)
        self.labelInfo.setEnabled(True)
        self.labelInfo.setGeometry(QtCore.QRect(10, 60, 281, 61))
        self.labelInfo.setObjectName("labelInfo")
        self.labelResult = QtWidgets.QLabel(self.centralwidget)
        self.labelResult.setGeometry(QtCore.QRect(300, 10, 111, 41))
        self.labelResult.setObjectName("labelResult")
        self.labelInfo.raise_()
        self.startVideo.raise_()
        self.labelResult.raise_()
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.startVideo.setText(_translate("MainWindow", "Получить видео"))
        self.labelInfo.setText(_translate("MainWindow", "Переводчик запущен. \n"
"Нажмите Q, чтобы закончить \n"
"Нажмите T, для захвата руки"))
        self.labelResult.setText(_translate("MainWindow", "text: NAN"))

