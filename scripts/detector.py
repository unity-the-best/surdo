import sys
import os
import time
import uuid
import math
from IPython.display import display as ipydisplay, Image, clear_output
import numpy as np
import cv2

(major_ver, minor_ver, subminor_ver) = (cv2.__version__).split('.')
import keras
from keras import backend as K
from keras.preprocessing.image import ImageDataGenerator, load_img, img_to_array
from keras.models import Sequential, load_model, model_from_json
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D

IMAGES_FOLDER = os.path.join('images')
MODEL_PATH = os.path.join('C:\/Users\Minatych\source\/repos\surdo\scripts\model')
print(MODEL_PATH)
MODEL_FILE = os.path.join(MODEL_PATH, 'mega_model.hdf5')
MODEL_HISTORY = os.path.join(MODEL_PATH, 'model_history.txt')
hand_model = load_model(MODEL_FILE, compile=False)
SAVING_PART_COUNT = 500

CURR_POS='you'

isSave = False

classes = {
    0: 'a',
    1: 'b',
    2: 'bi',
    3: 'e',
    4: 'ee',
    5: 'f',
    6: 'g',
    7: 'h',
    8: 'i',
    9: 'ia',
    10: 'l',
    11: 'm',
    12: 'n',
    13: 'o',
    14: 'p',
    15: 'r',
    16: 's',
    17: 'sh',
    18: 't',
    19: 'u',
    20: 'v',
    21: 'x',
    22: 'you'
}

def setup_tracker(ttype):
    tracker_types = ['BOOSTING', 'MIL', 'KCF', 'TLD', 'MEDIANFLOW', 'GOTURN']
    tracker_type = tracker_types[ttype]

    if int(minor_ver) < 3:
        tracker = cv2.Tracker_create(tracker_type)
    else:
        if tracker_type == 'BOOSTING':
            tracker = cv2.TrackerBoosting_create()
        if tracker_type == 'MIL':
            tracker = cv2.TrackerMIL_create()
        if tracker_type == 'KCF':
            tracker = cv2.TrackerKCF_create()
        if tracker_type == 'TLD':
            tracker = cv2.TrackerTLD_create()
        if tracker_type == 'MEDIANFLOW':
            tracker = cv2.TrackerMedianFlow_create()
        if tracker_type == 'GOTURN':
            tracker = cv2.TrackerGOTURN_create()
    return tracker


def mask_array(array, imask):
    if array.shape[:2] != imask.shape:
        raise Exception("Shapes of input and imask are incompatible")
    output = np.zeros_like(array, dtype=np.uint8)
    for i, row in enumerate(imask):
        output[i, row] = array[i, row]
    return output


video = cv2.VideoCapture(0)
if not video.isOpened():
    print("Could not open video")
    sys.exit()
ok, frame = video.read()
if not ok:
    print("Cannot read video")
    sys.exit()
bg = frame.copy()
kernel = np.ones((3, 3), np.uint8)
positions = {
    'hand_pose': (15, 40),
    'fps': (15, 20),
    'null_pos': (200, 200)
}
bbox_initial = (116, 116, 170, 170)
bbox = bbox_initial
tracking = -1

to_save_count = SAVING_PART_COUNT
delta_save = 5

while True:
    ok, frame = video.read()
    display = frame.copy()
    data_display = np.zeros_like(display, dtype=np.uint8)
    if not ok:
        break
    timer = cv2.getTickCount()
    diff = cv2.absdiff(bg, frame)
    mask = cv2.cvtColor(diff, cv2.COLOR_BGR2GRAY)
    th, thresh = cv2.threshold(mask, 10, 255, cv2.THRESH_BINARY)
    opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel)
    closing = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel)
    img_dilation = cv2.dilate(closing, kernel, iterations=2)
    imask = img_dilation > 0
    foreground = mask_array(frame, imask)
    foreground_display = foreground.copy()
    if tracking != -1:
        tracking, bbox = tracker.update(foreground)
        tracking = int(tracking)
    hand_crop = frame[int(bbox[1]):int(bbox[1] + bbox[3]), int(bbox[0]):int(bbox[0] + bbox[2])]
    hand_to_save = hand_crop.copy()
    hand_crop = cv2.cvtColor(hand_crop, cv2.COLOR_BGR2GRAY)
    try:
        hand_crop_resized = np.expand_dims(cv2.resize(hand_crop, (54, 54)), axis=0).reshape((1, 54, 54, 1))
        prediction = hand_model.predict(hand_crop_resized)
        predi = prediction[0].argmax()
        gesture = classes[predi]

        for i, pred in enumerate(prediction[0]):
            barx = positions['hand_pose'][0]
            bary = 60 + i * 60
            bar_height = 20
            bar_length = int(400 * pred) + barx
            if i == predi:
                colour = (0, 255, 0)
            else:
                colour = (0, 0, 255)

            cv2.putText(data_display, "{}: {}".format(classes[i], pred), (positions['hand_pose'][0], 30 + i * 60),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.75, (255, 255, 255), 2)
            cv2.rectangle(data_display, (barx, bary), (bar_length, bary - bar_height), colour, -1, 1)

        cv2.putText(display, "hand pose: {}".format(gesture), positions['hand_pose'], cv2.FONT_HERSHEY_SIMPLEX, 0.75,
                    (0, 0, 255), 2)
        cv2.putText(foreground_display, "hand pose: {}".format(gesture), positions['hand_pose'],
                    cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0, 0, 255), 2)
    except Exception as ex:
        cv2.putText(display, "hand pose: error", positions['hand_pose'], cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0, 0, 255), 2)
        cv2.putText(foreground_display, "hand pose: error", positions['hand_pose'], cv2.FONT_HERSHEY_SIMPLEX, 0.75,
                    (0, 0, 255), 2)
    p1 = (int(bbox[0]), int(bbox[1]))
    p2 = (int(bbox[0] + bbox[2]), int(bbox[1] + bbox[3]))
    cv2.rectangle(foreground_display, p1, p2, (255, 0, 0), 2, 1)
    cv2.rectangle(display, p1, p2, (255, 0, 0), 2, 1)
    hand_pos = ((p1[0] + p2[0]) // 2, (p1[1] + p2[1]) // 2)
    mouse_change = ((p1[0] + p2[0]) // 2 - positions['null_pos'][0], positions['null_pos'][0] - (p1[1] + p2[1]) // 2)
    cv2.circle(display, positions['null_pos'], 5, (0, 0, 255), -1)
    cv2.circle(display, hand_pos, 5, (0, 255, 0), -1)
    cv2.line(display, positions['null_pos'], hand_pos, (255, 0, 0), 5)
    fps = cv2.getTickFrequency() / (cv2.getTickCount() - timer)
    cv2.putText(foreground_display, "FPS : " + str(int(fps)), positions['fps'], cv2.FONT_HERSHEY_SIMPLEX, 0.65,
                (50, 170, 50), 2)
    cv2.putText(display, "FPS : " + str(int(fps)), positions['fps'], cv2.FONT_HERSHEY_SIMPLEX, 0.65, (50, 170, 50), 2)
    cv2.imshow("display", display)
    cv2.imshow("data", data_display)
    cv2.imshow("diff", diff)
    cv2.imshow("thresh", thresh)
    cv2.imshow("img_dilation", img_dilation)
    try:
        cv2.imshow("hand_crop", hand_crop)
    except:
        pass
    cv2.imshow("foreground_display", foreground_display)

    k = cv2.waitKey(1) & 0xff
    if k == 27:
        break  # нажат ESC
    elif k == 114 or k == 112:
        # нажата r
        bg = frame.copy()
        bbox = bbox_initial
        tracking = -1
    elif k == 116:
        # нажата t
        tracker = setup_tracker(2)
        tracking = tracker.init(frame, bbox)
    elif k == 115:
        # нажата s
        isSave = True
    elif k != 255:
        print(k)

    if isSave == True:
        if delta_save == 0:
            fname = os.path.join('C:/\/Users\Minatych\source\/repos\surdo\scripts\data', CURR_POS,
                                 "{}_{}.jpg".format(CURR_POS,
                                                    str(uuid.uuid4())))  # get_unique_name(os.path.join("data", CURR_POS))))
            print(fname)
            cv2.imwrite(fname, hand_to_save)
            delta_save = 5
            to_save_count -= 1
            if to_save_count == 0:
                to_save_count = SAVING_PART_COUNT
                isSave = False
        delta_save -= 1
cv2.destroyAllWindows()
video.release()
